from distutils.core import setup
from setuptools import find_packages

setup(
    name='Nexshop-theme',
    version='0.6',
    author='Nastya Kruglikova',
    author_email='nastya.kruglikova@gmail.com',
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
    entry_points={
        'sshop.plugins.settings.entrypoints': [
            'update_settings = nexshop_theme:update_settings',
            'install = nexshop_theme:install',
            'upgrade = nexshop_theme:upgrade',
        ],
    },
)
